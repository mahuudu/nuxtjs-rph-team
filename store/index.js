import Vuex from 'vuex';
import Cookies from 'js-cookie'

const createStore = () => {
    return new Vuex.Store({
        state : {
            decks : [],
            token : null
        },
        mutations: {
            setDecks(state,decks){
                state.decks = decks
            },
            setToken(state,token){
                state.token = token
            },
            clearToken(state){
                state.token = null;
            }
        },
        actions : {
            setDecks({commit}, decks){
                commit('setDecks', decks)
            },
            setToken({commit}, token){
                commit('setToken', token)
            },
            authenticateUser(vuexContext, payload){
                return new Promise((resolve,reject) =>{
                    let authUrlApi = `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${process.env.fbApiKey}`;

                    //if not login => register
                    if(!payload.isLogin){
                            authUrlApi = `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${process.env.fbApiKey}`;
                    }

        
                    this.$axios
                        .$post(
                        authUrlApi,
                        {
                            email: payload.email,
                            password: payload.password,
                            returnSecureToken: true,
                        }
                        )
                        .then((result) => {
                            vuexContext.commit('setToken', result.idToken)
                            Cookies.set('token', result.idToken)
                            Cookies.set('tokenExpiration', parseInt(result.expiresIn * 1000))
                            vuexContext.dispatch('setLogoutTimer',parseInt(result.expiresIn * 1000))
                            resolve({success: true})
                        })
                        .catch( (error) => reject(error.response))
                    
                })
           },
           setLogoutTimer(vuexContext, duration){
                setTimeout(() => {
                    vuexContext.commit('clearToken');
                }, duration);
           },
           initAuth(vuexContext,req){
               let token, tokenExpiration;

               if(req){
                   if(!req.headers.cookie) return false;
                   const tokenKey = req.headers.cookie.split(';',).find( c => c.trim().startWith('token='))
                   const tokenExpirationKey = req.headers.cookie.split(';',).find( c => c.trim().startWith('tokenExpiration='))

                   if(!tokenKey || !tokenExpirationKey) return false

                   token = tokenKey.split('=')[1]
                   tokenExpiration = tokenExpirationKey.split('=')[1] 
               }else{
                   return false;
               }

           },
           logOut(vuexContext){
               Cookies.remove('token')
               Cookies.remove('tokenExpiration')
               vuexContext.commit('clearToken')
           }

        },
        getters : {
            decks(state){
                return state.decks;
            },
            token(state){
                return state.token;
            },
            isAuthenticated(state){
                return state.token != null;
            }
        }
    })
}
export default createStore;