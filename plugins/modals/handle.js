import Layout from './layout';

const vModal = {
    install(Vue){
        this.EventBus = new Vue();

        Vue.component('v-modal', Layout);

        Vue.prototype.$modal = {
            open(params){
                vModal.EventBus.$emit('open', params)
            },
            close(params){
                vModal.EventBus.$emit('close', params);
            }
        }
    }
}

export default vModal;